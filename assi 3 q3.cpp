#include <stdio.h>

int main(){
	
char x;
// uc is stands for upper case character and lc is stands for lower case character
int uc, lc;

printf("Enter a character: ");
scanf("%c", &x);

uc = (x == 'A' || x == 'E' || x == 'I' || x == 'O' || x == 'U');
lc = (x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u');

if(uc || lc){
	printf("Vowel");
}else
	printf("Consonant");
	
	

}
